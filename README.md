### How to run the app
Open a terminal and:

1. Clone the repo `git clone https://anarchos78@bitbucket.org/anarchos78/tails-test.git`  
2. Go into the application directory `cd tails-test`  
3. Create a virtual environment for the project, using python 3.6.x and activate it (I prefer ***pyenv***)  
4. While in the activated virtual environment run `pip install -r requirements.txt` to install the required packages  
5. To run the tests in the projects directory issue the command `python -W ignore:ResourceWarning -m unittest discover`  
6. Run the server: python run.py  
7. 
Open a terminal and issue the following to get an invoice in GBP:
```sh
curl -H "Content-Type: application/json" \
-X POST \
-d '{"order":{"id":12345,"customer":{"f_name":"Athanasios","l_name":"Rigas","more":"attributes"},"items":[{"product_id":1,"quantity":1},{"product_id":2,"quantity":5},{"product_id":3,"quantity":1}]}}' \
http://127.0.0.1:5000/products/v1/invoice
```  
or issue the following to get an invoice  in EUR:
```sh
curl -H "Content-Type: application/json" \
-X POST \
-d '{"order":{"id":12345,"customer":{"f_name":"Athanasios","l_name":"Rigas","more":"attributes"},"items":[{"product_id":1,"quantity":1},{"product_id":2,"quantity":5},{"product_id":3,"quantity":1}]}}' \
http://127.0.0.1:5000/products/v1/invoice?currency=eur
```  
You can change the URL argument "currency" to any ISO currency code as long as it's supported by [fixer.io](http://fixer.io/).

**Enjoy!**
