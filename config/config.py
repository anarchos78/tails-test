# -*- coding: utf-8 -*-

"""
.. module:: .config.py
   :synopsis: <write a synopsis>

.. moduleauthor:: Athanasios Rigas
                  <admin@ithemis.gr>
"""

import logging

from products.api import products


class BaseConfig(object):
    """
    Base configuration class.
    Here you can define common configuration
    that other config classes will inherit from
    """
    DEBUG = False
    TESTING = False
    LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    LOGGING_LOCATION = 'my-awesome-app.log'
    LOGGING_LEVEL = logging.INFO


class DevConfig(BaseConfig):
    """
    Development configuration class
    """
    DEBUG = True
    TESTING = True


class TestConfig(BaseConfig):
    """
    Test configuration class
    """
    DEBUG = False
    TESTING = True


class ProdConfig(BaseConfig):
    """
    Test Production class
    """
    DEBUG = False
    TESTING = False


def configure_app(app):
    """
    App configuration
    """
    app.config.from_object('config.config.ProdConfig')

    app.register_blueprint(products)

    # Configure logging
    handler = logging.FileHandler(app.config['LOGGING_LOCATION'])
    handler.setLevel(app.config['LOGGING_LEVEL'])
    formatter = logging.Formatter(app.config['LOGGING_FORMAT'])
    handler.setFormatter(formatter)
    app.logger.addHandler(handler)
