# -*- coding: utf-8 -*-

"""
.. module:: .api.py
   :synopsis: <write a synopsis>

.. moduleauthor:: Athanasios Rigas
                  <admin@ithemis.gr>
"""

import os

from decimal import Decimal, ROUND_HALF_UP

from flask import (
    Blueprint, jsonify, request,
    send_from_directory, current_app, make_response
)

from .utils import ParseLargePriceFile, convert_currency

products = Blueprint(
    'products',
    __name__,
    url_prefix='/products/v1',  # API version
    static_folder='static'
)


@products.route('/favicon.ico')
def favicon():
    return send_from_directory(
        os.path.join(products.root_path, 'static'),
        'favicon.ico',
        mimetype='image/vnd.microsoft.icon'
    )


@products.route('/invoice', methods=['GET', 'POST'])
def get_invoice():
    """Returns an order"""

    if request.method == 'GET':
        return make_response(jsonify({'error': 'Method Not Allowed'}), 405)
    else:
        if not request.get_json() or 'order' not in request.get_json():
            current_app.logger.warning('Not the correct payload')

            return make_response(jsonify({'error': 'Bad request'}), 400)

        data = request.get_json()

        order = data.get('order')
        order_id = order.get('id')

        if order_id != 12345:
            current_app.logger.warning(f'Order number "{order_id}" not found')

            return make_response(
                jsonify({'error': f'Order number "{order_id}" not found'}), 404
            )
        else:
            try:
                invoice_items = [
                    {
                        'product_id': item.get('product_id'),
                        'price': (
                            ParseLargePriceFile(
                                product_id=item.get('product_id')
                            ).get_price_vat().get('price') *
                            item.get('quantity')
                        ),
                        'vat': int(
                            Decimal(
                                ParseLargePriceFile(
                                    product_id=item.get('product_id')
                                ).get_price_vat().get('vat') *
                                ParseLargePriceFile(
                                    product_id=item.get('product_id')
                                ).get_price_vat().get('price') *
                                item.get('quantity')
                            ).quantize(Decimal('1'), rounding=ROUND_HALF_UP)
                        )
                    }
                    for item
                    in order.get('items')
                ]

                invoice = {
                    'invoice': {
                        'total_price': sum(
                            [
                                item.get('price')
                                for item
                                in invoice_items
                            ]
                        ),
                        'total_vat': sum(
                            [
                                item.get('vat')
                                for item
                                in invoice_items
                            ]
                        ),
                        'items': invoice_items
                    }
                }

                currency = request.args.get('currency')

                if currency:
                    invoice = {
                        'invoice': {
                            'total_price': sum(
                                [
                                    convert_currency(
                                        currency=currency,
                                        amount=item.get('price')
                                    )
                                    for item
                                    in invoice_items
                                ]
                            ),
                            'total_vat': sum(
                                [
                                    convert_currency(
                                        currency=currency,
                                        amount=item.get('vat')
                                    )
                                    for item
                                    in invoice_items
                                ]
                            ),
                            'items': [
                                {
                                    'product_id': item.get('product_id'),
                                    'price': convert_currency(
                                        currency=currency,
                                        amount=item.get('price')
                                    ),
                                    'vat': convert_currency(
                                        currency=currency,
                                        amount=item.get('vat')
                                    )
                                }
                                for item
                                in invoice_items
                            ]
                        }
                    }

                return jsonify(invoice)
            except AttributeError as err:
                current_app.logger.error(
                    f'{err} -- Check the payload for item[s] '
                    f'that doesn\'t/don\'t exist[s] in the price.json'
                )

                return make_response(
                    jsonify(
                        {
                            'error': (
                                'The execution of the '
                                'service failed in some way'
                            )
                        }
                    ),
                    500
                )
