# -*- coding: utf-8 -*-

"""
.. module:: .app.py
   :synopsis: <write a synopsis>

.. moduleauthor:: Athanasios Rigas
                  <admin@ithemis.gr>
"""

from flask import Flask

from config.config import configure_app


def create_app():
    app = Flask(__name__)
    configure_app(app)

    return app
