# -*- coding: utf-8 -*-

"""
.. module:: .test_api.py
   :synopsis: <write a synopsis>

.. moduleauthor:: Athanasios Rigas
                  <admin@ithemis.gr>
"""

import json
import unittest

from products.app import create_app


class ApiTests(unittest.TestCase):
    """Tests the products API"""

    def setUp(self):
        """Setup"""

        self.app = create_app()

        self.app.config['TESTING'] = True
        self.app.config['DEBUG'] = False
        self.app.config['JSONIFY_PRETTYPRINT_REGULAR'] = False

        self.app = self.app.test_client()

        self.order_data = {
            "order": {
                "id": 12345,
                "customer": {
                    "f_name": "Athanasios",
                    "l_name": "Rigas",
                    "more": "attributes"
                },
                "items": [
                    {
                        "product_id": 1,
                        "quantity": 1
                    },
                    {
                        "product_id": 2,
                        "quantity": 5
                    },
                    {
                        "product_id": 3,
                        "quantity": 1
                    }
                ]
            }
        }

    def tearDown(self):
        """Clean up"""

        pass

    def test_favicon(self):
        """Tests favicon endpoint"""

        result = self.app.get('/products/v1/favicon.ico')

        # assert the status code of the response
        self.assertEqual(result.status_code, 200)

    def test_get_invoice(self):
        """Tests the get_invoice endpoint"""

        response_gbp = self.app.post(
            '/products/v1/invoice',
            data=json.dumps(self.order_data),
            content_type='application/json'
        )

        invoice_data = str(response_gbp.data)

        self.assertIn('invoice', invoice_data)
        self.assertIn('total_price', invoice_data)
        self.assertIn('total_vat', invoice_data)


if __name__ == "__main__":
    unittest.main()
