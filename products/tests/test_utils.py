# -*- coding: utf-8 -*-

"""
.. module:: .test_utils.py
   :synopsis: <write a synopsis>

.. moduleauthor:: Athanasios Rigas
                  <admin@ithemis.gr>
"""

import os
import requests
import unittest

from decimal import Decimal, ROUND_HALF_UP

from products.app import create_app
from products.utils import ParseLargePriceFile, convert_currency


class TestParseLargePriceFile(unittest.TestCase):
    """Test ParseLargePriceFile"""

    def setUp(self):
        """Setup"""

        self.app = create_app()

        self.app.config['TESTING'] = True
        self.app.config['DEBUG'] = False
        self.app.config['JSONIFY_PRETTYPRINT_REGULAR'] = False

        self.app = self.app.test_client()

        self.file = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            '../',
            'data',
            'pricing.json'
        )

        self.product_id = 5
        self.obj = ParseLargePriceFile(product_id=self.product_id)

    def tearDown(self):
        """Clean up"""

        pass

    def test__get_vat(self):
        """Tests _get_vat method"""

        self.assertEquals(0.2, float(self.obj._get_vat('standard')))
        self.assertEquals(0, float(self.obj._get_vat('zero')))

    def test_get_price_vat(self):
        """Tests get_price_vat method"""

        self.assertIn('price', self.obj.get_price_vat())
        self.assertIn('vat', self.obj.get_price_vat())
        self.assertEquals(1250, self.obj.get_price_vat().get('price'))
        self.assertEquals(0.2, self.obj.get_price_vat().get('vat'))


class TestConvertCurrency(unittest.TestCase):
    """Test convert_currency function"""

    def setUp(self):
        """Setup"""

        self.app = create_app()

        self.app.config['TESTING'] = True
        self.app.config['DEBUG'] = False
        self.app.config['JSONIFY_PRETTYPRINT_REGULAR'] = False

        self.app = self.app.test_client()

        self.amount = 400
        self.base = 'gbp'
        self.currency = 'jpy'

    def tearDown(self):
        """Clean up"""

        pass

    def test_convert_currency(self):
        """Tests convert_currency method"""

        # Passing the base currency (GBP)
        # should equal to the amount (self.amount)
        self.assertEquals(
            400, convert_currency(currency='gbp', amount=self.amount)
        )

        response = requests.get(
            'https://api.fixer.io/latest',
            params={'base': self.base}
        )

        currency_rate = response.json().get('rates').get(self.currency.upper())

        currency_rate_rounded = int(
            Decimal(currency_rate)
            .quantize(Decimal('1'), rounding=ROUND_HALF_UP)
        )

        # Check other currency
        self.assertEquals(
            currency_rate_rounded,
            convert_currency(currency=self.currency, amount=1)
        )
