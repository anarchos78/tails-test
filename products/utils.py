# -*- coding: utf-8 -*-

"""
.. module:: .utils.py
   :synopsis: <write a synopsis>

.. moduleauthor:: Athanasios Rigas
                  <admin@ithemis.gr>
"""

import os

from decimal import Decimal, ROUND_HALF_UP
from flask import current_app

import ijson
import requests
import requests_cache


class ParseLargePriceFile(object):
    """
    Class that has helper methods for parsing the price file.
    """

    def __init__(self, file=None, product_id=None):
        """The constructor"""

        if file is None:
            self.file = os.path.join(
                os.path.dirname(os.path.realpath(__file__)),
                '../',
                'data',
                'pricing.json'
            )
        self.product_id = product_id

    def _get_vat(self, vat_band=None):
        """Return vat of a product"""

        with open(self.file) as fh:
            # using ijson help us to read large files efficiently
            vat_bands = ijson.items(fh, 'vat_bands')

            for band in vat_bands:
                try:
                    return band[vat_band]
                except KeyError:
                    current_app.logger.info(f'Vat band "{vat_band}" not found')

    def get_price_vat(self):
        """Return price and VAT of a product"""

        with open(self.file) as fh:
            prices = ijson.items(fh, 'prices.item')

            for item in prices:
                if item['product_id'] == self.product_id:
                    return {
                        'price': item['price'],
                        'vat': float(self._get_vat(item['vat_band']))
                    }

            current_app.logger.info(
                f'product_id "{self.product_id}" not found'
            )


def convert_currency(base='gbp', currency=None, amount=None):
    """
    Returns amount converted to the given currency
    :param base: <str> the base currency
    :param currency: <str> currency to convert to in ISO eg. EUR
    :param amount: <int> amount to be converted in cents
    :return: <int> the amount in cents, converted to the new currency
    """

    # Cache fixer.io result
    # In a real case scenario we would use a cache engine like Redis
    requests_cache.install_cache(
        expire_after=86400  # cache for one day
    )

    url = 'https://api.fixer.io/latest'
    response = None

    try:
        response = requests.get(url, params={'base': base})
        response.raise_for_status()
    except requests.exceptions.HTTPError as eh:
        current_app.logging.error("Http Error:", eh)
    except requests.exceptions.ConnectionError as ec:
        current_app.logging.error("Connection Error:", ec)
    except requests.exceptions.Timeout as et:
        current_app.logging.error("Timeout Error:", et)
    except requests.exceptions.RequestException as er:
        current_app.logging.error("Something Else: ", er)

    rates = response.json().get('rates')

    if currency.upper() == 'GBP':
        currency_rate = 1
    else:
        currency_rate = rates.get(currency.upper())

    if currency_rate is None:
        current_app.logger.warning(f'Currency "{currency}" not found')
    else:
        if amount is None:
            current_app.logger.warning('Oops! You have forgotten the amount!')
        else:
            converted_not_rounded = amount * currency_rate

            converted_rounded = int(
                Decimal(converted_not_rounded)
                .quantize(Decimal('1'), rounding=ROUND_HALF_UP)
            )

            return converted_rounded
