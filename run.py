# -*- coding: utf-8 -*-

"""
.. module:: .run.py
   :synopsis: <write a synopsis>

.. moduleauthor:: Athanasios Rigas
                  <admin@ithemis.gr>
"""

from products.app import create_app

if __name__ == '__main__':
    application = create_app()
    application.run()
